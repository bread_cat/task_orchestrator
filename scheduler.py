import importlib.util
import sys
import os
from datetime import datetime
import time

if __name__ == '__main__':
    for pipe_py in os.listdir(sys.argv[-1]):
        full_path = os.path.join(sys.argv[-1], pipe_py)
        module = pipe_py.replace('.py', '')
        spec = importlib.util.spec_from_file_location(module, full_path)
        foo = importlib.util.module_from_spec(spec)
        sys.modules[module] = foo
        spec.loader.exec_module(foo)
        pipeline = foo.pipeline

        schedule_iter = pipeline.get_cron_iterable()
        first = schedule_iter.get_next(ret_type=datetime)
        second = schedule_iter.get_next(ret_type=datetime)
        interval = (second - first).total_seconds()
        while True:
            pipeline.run()
            time.sleep(interval)
