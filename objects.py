# create abstract class Task

from abc import ABC, abstractmethod
from typing import Callable, Optional
import networkx as nx
import croniter


class Task:
    def __init__(self, name: str, pipeline: "Pipeline"):
        self.name = name
        self.pipeline = pipeline
        self.upstream: Optional[Task] = None
        self.downstream: Optional[Task] = None

    def set_downstream(self, task: "Task"):
        self.downstream = task
        self.pipeline.add_task(self, task)

    def set_upstream(self, task: "Task"):
        self.upstream = task
        self.pipeline.add_task(task, self)

    @abstractmethod
    def run(self):
        pass

    def __str__(self):
        return self.name

    def __repr__(self):
        return f"Task({self.name})"


class PythonTask(Task):
    def __init__(self, name: str, python_callable: Callable, pipeline: "Pipeline"):
        super().__init__(name, pipeline)
        self.python_callable = python_callable

    def run(self):
        self.python_callable()


class Pipeline:
    def __init__(self, name: str, schedule: str):
        self.name = name
        self.schedule = schedule
        self.graph = nx.OrderedDiGraph()

    def get_cron_iterable(self):
        return croniter.croniter(self.schedule)

    def add_task(self, task1: "Task", task2: "Task"):
        self.graph.add_edge(task1, task2)

    def retrieve_graph_path(self):
        nodes = nx.topological_sort(self.graph)
        return nodes

    def run(self):
        for task in self.retrieve_graph_path():
            task.run()
