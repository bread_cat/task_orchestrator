import asyncio
import time
from datetime import datetime

from objects import Pipeline, PythonTask
import asyncio

pipeline = Pipeline(
    name='test',
    schedule='* * * * *'
)


def print_hello():
    print('hello')


def print_world():
    print('world')

def print_time():
    print(datetime.now())


task_1 = PythonTask(
    'print_hello',
    python_callable=print_hello,
    pipeline=pipeline
)

task_2 = PythonTask(
    'print_world',
    python_callable=print_world,
    pipeline=pipeline
)

task_3 = PythonTask(
    'print_time',
    python_callable=print_time,
    pipeline=pipeline
)

task_1.set_downstream(task_2)
task_1.set_upstream(task_3)

# if __name__ == '__main__':
#     schedule_iter = pipeline.get_cron_iterable()
#     first = schedule_iter.get_next(ret_type=datetime)
#     second = schedule_iter.get_next(ret_type=datetime)
#     interval = (second - first).total_seconds()
#     while True:
#         pipeline.run()
#         time.sleep(interval)
