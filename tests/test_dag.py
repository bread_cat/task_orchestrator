from objects import Pipeline, Task
import pytest


def test_dag_unordered_insert():
    pipeline = Pipeline("test", "test")
    # create tasks
    task1 = Task("task1", pipeline)
    task2 = Task("task2", pipeline)
    task3 = Task("task3", pipeline)
    task4 = Task("task4", pipeline)

    # set upstreams
    task1.set_upstream(task2)
    task3.set_upstream(task1)
    task3.set_downstream(task4)

    nodes = pipeline.retrieve_graph_path()

    assert nodes == ['task2', 'task1', 'task3', 'task4']
